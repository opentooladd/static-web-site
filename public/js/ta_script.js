/**
 ** Simple script for basic visual actions
 ** TODO : object oriented programming
 **/

$(function() {	
	// http://slicknav.com/
	// include slicknav menu
	$('#menu').slicknav({
		label: "menu",
		appendTo: "header",
		closeOnClick: true,		
	});

	// calculate pages custom sizes
	function refreshPages() {				
		// full page slider
		$('#ta_slider').css("height", (window.innerHeight) + "px");
		$('#ta_slider').css("background-image", 'url(media/image/ardoise.jpg)');
		$('#ta_slider').css("background-size", "100% " + window.innerHeight + "px");
		$('#ta_slider').css("background-repeat", "no-repeat");

		// article height at least window height
		$('#article').css("min-height", (window.innerHeight - $("header").height()) + "px");

		// all description column at the same height
		$('article').each( function() {
			// ensure dynamic height calculation.
			$(this).children("div.column").height("auto");

			var maxColumnHeight = 0;
			$(this).children("div.column").each(function() {
				var thisHeight = eval($(this).height());						
				maxColumnHeight = (thisHeight < maxColumnHeight) ? maxColumnHeight : thisHeight;
			});

			$(this).children("div.column").height(maxColumnHeight);
		});
	}

	// adapt header according position
	function refreshHeader() {
		var topWindowPos = $(window).scrollTop();
		var bottomSlidePos = $("#ta_slider").offset().top + $("#ta_slider").height();
		var headerHeight = $("header").height();

		if( ($("#ta_slider").css("display")!="none") && (topWindowPos < bottomSlidePos - headerHeight/2) ) {
			$( "header" ).css( "background", "none" );
			$( "header h2" ).css( "color", "white" );
		} else {
			$( "header" ).css( "background-color", "#ECECEC");
			$( "header h2" ).css( "color", "black" );
		}
	}

	// size event needing to update some elements
	
	$(window).load(function() {
		$('#menu').slicknav('close'); 
		refreshPages();
		refreshHeader();
	});

	$(window).scroll(function() {
		$('#menu').slicknav('close'); 
		refreshHeader();
	});

	$(window).resize(function() {
		refreshPages();
		refreshHeader(); // don't forget lateral resize
	});

});
